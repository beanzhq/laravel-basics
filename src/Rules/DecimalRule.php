<?php

namespace Beanz\Basics\Rules;

use Illuminate\Contracts\Validation\Rule;
use InvalidArgumentException;

class DecimalRule implements Rule
{
    /** @var int */
    private $wholeNumbers;

    /** @var int */
    private $decimalNumbers;

    /** @var bool */
    private $allowIntegers;

    /**
     * @throws InvalidArgumentException
     */
    public function __construct(int $wholeNumbers = 8, int $decimalNumbers = 2, bool $allowIntegers = true)
    {
        if ($wholeNumbers < 1 || $decimalNumbers < 1) {
            throw new InvalidArgumentException();
        }
        $this->wholeNumbers = $wholeNumbers;
        $this->decimalNumbers = $decimalNumbers;
        $this->allowIntegers = $allowIntegers;
    }

    /** {@inheritdoc} */
    public function passes($attribute, $value): bool
    {
        if (!is_string($value) && !is_numeric($value)) {
            return false;
        }

        $decimalsOptional = $this->allowIntegers ? '?' : '';

        return preg_match("/^\d{1,{$this->wholeNumbers}}(\.\d{1,{$this->decimalNumbers}})$decimalsOptional$/", $value);
    }

    public function message(): string
    {
        return trans('validationRules::validation.decimal');
    }
}
