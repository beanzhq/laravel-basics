<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Ramsey\Uuid\Uuid;

class UuidRule implements Rule
{
    /**
     * {@inheritdoc}
     */
    public function passes($attribute, $uuid): bool
    {
        return Uuid::isValid($uuid);
    }

    public function message(): string
    {
        return trans('validationRules::validation.uuid');
    }
}
