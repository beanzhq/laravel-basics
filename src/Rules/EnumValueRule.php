<?php

namespace Beanz\Basics\Rules;

use Beanz\Basics\Enum;
use Illuminate\Contracts\Validation\Rule;

class EnumValueRule implements Rule
{
    /** @var @string */
    protected $attribute;

    /** @var Enum */
    protected $enum;

    public function __construct(string $enumClass)
    {
        $this->enum = $enumClass;
    }

    /**
     * {@inheritdoc}
     */
    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;

        return $this->enum::validateValue($value);
    }

    public function message(): string
    {
        $validValues = implode(', ', $this->enum::getValues());

        return trans('validationRules::validation.enum-value', [
            'attribute' => $this->attribute,
            'validValues' => $validValues,
        ]);
    }
}
