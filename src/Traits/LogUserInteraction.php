<?php

namespace Beanz\Basics\Traits;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait LogUserInteraction
{
    /** @var bool */
    protected static $logUserInteraction = true;

    /**
     * Creator field in the database.
     *
     * @var string
     */
    protected static $creator_id = 'creator_id';

    /**
     * Updater field in the database.
     *
     * @var string
     */
    protected static $updater_id = 'updater_id';

    /**
     * Observe create, update and save events on the model and set created_by and updated_by values to current user.
     */
    protected static function bootLogUserInteraction(): void
    {
        static::creating(function ($model): void {
            if (self::$logUserInteraction) {
                self::setFieldToUserId($model, static::$creator_id);
            }
        });

        static::saving(function ($model): void {
            if (self::$logUserInteraction) {
                self::setFieldToUserId($model, static::$updater_id);
            }
        });
    }

    public function setLogUserInteraction(bool $enabled): void
    {
        self::$logUserInteraction = $enabled;
    }

    /**
     * Set the value of the field to the current user's id.
     */
    private static function setFieldToUserId(Model $model, string $fieldName): void
    {
        $user = Auth::user();
        $model->$fieldName = $user->id ?? null;
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo('App\\Models\\User', static::$creator_id);
    }

    public function updater(): BelongsTo
    {
        return $this->belongsTo('App\\Models\\User', static::$updater_id);
    }

    public function getCastsLogUserInteraction(): array
    {
        return [
            static::$creator_id => 'integer',
            static::$updater_id => 'integer',
        ];
    }
}
