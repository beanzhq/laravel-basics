<?php

namespace Beanz\Basics\Traits;

use Illuminate\Database\Eloquent\Model;

trait Uid
{
    /** @var int */
    protected static $uid_field = 'uid';

    /**
     * Initialize the trait. This method is called by the abstract Model during the 'boot' method.
     * The initialization will add listener on the 'creating' event,
     * so a uid can be inserted before the model is saved in the database.
     */
    protected static function bootUid(): void
    {
        static::creating(function ($model): void {
            self::setUidField($model);
        });
    }

    /**
     * Get the uid column for visible property
     */
    public function getVisibleUid(): array
    {
        return [static::$uid_field];
    }

    /**
     * When a new model instance without uid is created, the uid field should be set automatically.
     */
    protected static function setUidField(Model $model): void
    {
        if (empty($model->getAttribute(static::$uid_field))) {
            // Object has no UID yet, generate one.
            $model->setAttribute(static::$uid_field, uuid());
        }
    }

    public function getRouteKeyName(): string
    {
        return static::$uid_field;
    }
}
