<?php

namespace Beanz\Basics\Repositories;

use Beanz\Basics\Model;

interface RepositoryInterface
{
    public function save(Model $model): bool;
    public function create(array $attributes): Model;
    public function update(Model $model, array $attributes): bool;
    public function delete(Model $model): ?bool;
    public function restore(Model $model): ?bool;
    public function forceDelete($modelOrBuilder): int;
}
