<?php

namespace Beanz\Basics\Repositories;

use Beanz\Basics\Model;

abstract class Repository implements RepositoryInterface
{
    /** @var Model */
    protected $model;

    /** @var CacheRepository */
    protected $cacheRepository;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function setCacheRepository(CacheRepository $cacheRepository): void
    {
        $this->cacheRepository = $cacheRepository;
    }

    public function save(Model $model): bool
    {
        return $model->save();
    }

    public function create(array $data): Model
    {
        return $this->model->newInstance()->create($data);
    }

    public function update(Model $model, array $data): bool
    {
        return $model->update($data);
    }

    public function delete(Model $model): ?bool
    {
        return $model->delete();
    }

    public function restore(Model $model): ?bool
    {
        return $model->restore();
    }

    public function forceDelete($modelOrBuilder): int
    {
        return (int) $modelOrBuilder->forceDelete();
    }
}
