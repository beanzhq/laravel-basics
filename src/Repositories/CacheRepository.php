<?php

namespace Beanz\Basics\Repositories;

use Beanz\Basics\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

abstract class CacheRepository implements RepositoryInterface
{
    private const KEY_GLUE = '&';

    /** @var Repository */
    protected $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function save(Model $model): bool
    {
        $result = $this->repository->save($model);

        $this->flush($model);

        return $result;
    }

    public function create(array $data): Model
    {
        $model = $this->repository->create($data);

        $this->flush($model);

        return $model;
    }

    public function update(Model $model, array $data): bool
    {
        $result = $this->repository->update($model, $data);

        $this->flush($model);

        return $result;
    }

    public function delete(Model $model): ?bool
    {
        $result = $this->repository->delete($model);

        $this->flush($model);

        return $result;
    }

    public function restore(Model $model): ?bool
    {
        $result = $this->repository->restore($model);

        $this->flush($model);

        return $result;
    }

    public function forceDelete($modelOrBuilder): int
    {
        $result = $this->repository->forceDelete($modelOrBuilder);

        if ($modelOrBuilder instanceof Builder) {
            foreach ($modelOrBuilder->get() as $model) {
                $this->flush($model);
            }

            return $result;
        }

        $this->flush($modelOrBuilder);

        return $result;
    }

    /**
     * Get the cache tags of the current repository.
     */
    protected function getCacheTags(Model $model = null): array
    {
        $tags = ['repository:' . static::class];

        try {
            $tags[] = 'tenant:' . tenant()->uid;
        } catch (InvalidTenantException $ex) {
            Log::error($ex->getMessage());
        }

        return $tags;
    }

    /**
     * Returns the key string based on the given array.
     */
    protected function getCacheKey(array $keys): string
    {
        return implode(self::KEY_GLUE, $keys);
    }

    /**
     * Flush the cache using a cache tag.
     */
    abstract protected function flush(?Model $model = null): void;
}
