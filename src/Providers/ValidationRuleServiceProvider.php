<?php

namespace Beanz\Basics\Providers;

use Illuminate\Support\ServiceProvider;

class ValidationRuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/Resources/lang' => resource_path('lang/vendor/validationRules'),
        ]);

        $this->loadTranslationsFrom(__DIR__ . '/Resources/lang/', 'validationRules');
    }
}
