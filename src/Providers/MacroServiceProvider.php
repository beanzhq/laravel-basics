<?php

namespace Beanz\Basics\Providers;

use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register(): void
    {
        require_once __DIR__ . '/Macros/blueprint.php';
        require_once __DIR__ . '/Macros/collection.php';
    }
}
