<?php

return [
    'decimal' => 'This is not a valid decimal.',
    'enum-value' => 'This is not a valid enum value.',
    'uuid' => 'This is not a valid uuid.',
];
