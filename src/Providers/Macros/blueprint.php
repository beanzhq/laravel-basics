<?php

use Illuminate\Database\Schema\Blueprint;

Blueprint::macro('logUserInteraction', function (): void {
        $this->unsignedBigInteger('creator_id')->nullable();
        $this->unsignedBigInteger('updater_id')->nullable();
        $this->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
        $this->foreign('updater_id')->references('id')->on('users')->onDelete('set null');
});
