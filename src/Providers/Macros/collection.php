<?php

use Beanz\Basics\Services\PaginateService;
use Illuminate\Support\Collection;

Collection::macro('paginate', function (int $pageSize = 10) {
    return PaginateService::paginate($this, $this->count(), $pageSize);
});
