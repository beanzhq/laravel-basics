<?php

namespace Beanz\Basics;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use InvalidArgumentException;
use ReflectionClass;

abstract class Enum implements Arrayable
{
    /**
     * Associative array of enum instances (one per possible enum).
     *
     * @var array
     */
    protected static $enumMapping = [];

    /** @var string */
    protected $name;

    /** @var int|string */
    protected $value;

    /**
     * @param string $name
     * @param int|string $value
     */
    protected function __construct(string $name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @param int|string $value
     *
     * @throws InvalidArgumentException Thrown when the int value doesn't represent an enum instance.
     *
     * @return static
     */
    public static function fromValue($value): self
    {
        $instances = static::getInstances();

        if (!isset($instances[$value])) {
            throw new InvalidArgumentException('Invalid enum value', [
                'class' => get_called_class(),
                'value' => $value,
            ]);
        }

        return $instances[$value];
    }

    /**
     * @param string|null $name
     *
     * @throws InvalidArgumentException Thrown when the int value doesn't represent an enum instance.
     *
     * @return static
     */
    public static function fromName(?string $name)
    {
        $instances = static::getInstancesByName();
        $name = Str::lower($name);

        if (!isset($instances[$name])) {
            throw new InvalidArgumentException('Invalid enum name', [
                'class' => get_called_class(),
                'name' => $name,
            ]);
        }

        return $instances[$name];
    }

    protected static function initializeMapping(): void
    {
        $className = get_called_class();

        if (!isset(static::$enumMapping[$className])) {
            $reflectionClass = new ReflectionClass($className);
            $constants = $reflectionClass->getConstants();

            static::$enumMapping[$className] = [
                'names' => [],
                'values' => [],
            ];

            foreach ($constants as $name => $value) {
                $name = Str::lower($name);
                $enum = new static($name, $value);

                static::$enumMapping[$className]['names'][$name] = $enum;
                static::$enumMapping[$className]['values'][$value] = $enum;
            }
        }
    }

    /**
     * @return int|string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function getKey(): int
    {
        return $this->value;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHumanReadableName(): string
    {
        return trans($this->getTranslationKey());
    }

    public function getTranslationKey(): string
    {
        return 'enums.' . Str::kebab(class_basename(static::class)) . '.' . Str::lower($this->getName());
    }

    public function toArray(): array
    {
        return [
            'value' => $this->getValue(),
            'name' => $this->getHumanReadableName(),
        ];
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    public static function getValues(): array
    {
        return array_keys(static::getInstances());
    }

    public static function getNames(): array
    {
        return array_keys(static::getInstancesByName());
    }

    /**
     * Will return an array with all the possible instance.
     * Grouped by names and values.
     *
     * @return array<string, array>
     */
    protected static function getAllInstances(): array
    {
        $className = get_called_class();

        if (!isset(static::$enumMapping[$className])) {
            static::initializeMapping();
        }

        return static::$enumMapping[$className];
    }

    /**
     * Will return an array with all the possible instance.
     * Grouped by name.
     *
     * @return static[]
     */
    public static function getInstancesByName(): array
    {
        return static::getAllInstances()['names'];
    }

    /**
     * Will return an array with all the possible instance.
     * Grouped by value.
     *
     * @return static[]
     */
    public static function getInstances(): array
    {
        return static::getAllInstances()['values'];
    }

    public static function getSelectFields(): Collection
    {
        return collect(self::getInstances())
            ->map(function (self $instance) {
                return [
                    'text' => $instance->getHumanReadableName(),
                    'value' => $instance->getValue(),
                ];
            })
            ->values();
    }

    public static function validateName(string $name, ?string $className = null): bool
    {
        $enum = self::getFullQualifiedClassName($className);

        return in_array(Str::lower($name), $enum::getNames());
    }

    public static function validateValue(string $value, ?string $className = null): bool
    {
        $enum = self::getFullQualifiedClassName($className);

        return in_array($value, $enum::getValues());
    }

    private static function getFullQualifiedClassName(?string $className)
    {
        if ($className === null) {
            return get_called_class();
        }

        if (class_exists($className)) {
            return $className;
        }

        throw new InvalidArgumentException(trans('Invalid enum', [
            'class' => $className,
        ]));
    }

    public static function isValueFromName(?string $name, string $valueName): bool
    {
        $enumClassName = explode('::', $valueName)[0];
        try {
            $enum = forward_static_call([static::getFullQualifiedClassName($enumClassName), 'fromName'], $name);

            return $enum->getValue() === constant(__NAMESPACE__ . '\\' . $valueName);
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }

    public static function __callStatic(string $method, array $parameters)
    {
        return static::fromName(Str::snake($method));
    }

    public static function __set_state(array $attributes): self
    {
        return self::fromValue($attributes['value']);
    }
}
