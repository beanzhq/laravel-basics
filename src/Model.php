<?php

namespace Beanz\Basics;

use Exception;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;
use Schema;

/**
 * @method static bool insert($query, $bindings = array())
 * @method static Builder distinct()
 * @method static Builder getQuery()
 * @method static Builder inRandomOrder($seed = '')
 * @method static Builder join($table, $first, $operator = null, $second = null, $type = 'inner', $where = false)
 * @method static Builder orderBy($column, $direction = 'asc')
 * @method static Builder removedScopes()
 * @method static Builder select($columns = array())
 * @method static Builder when($value, $callback, $default = null)
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static Builder whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static Builder whereNull($column, $boolean = 'and', $not = false)
 * @method static Builder withCount($relations)
 * @method static Builder withGlobalScope($identifier, $scope)
 * @method static Builder withoutGlobalScope($scope)
 * @method static Builder withoutGlobalScopes($scopes = null)
 * @method static Builder|static whereNotExists($callback, $boolean = 'and')
 * @method static Builder|static whereNotNull($column, $boolean = 'and')
 * @method static Collection pluck($column, $key = null)
 * @method static Model findOrFail($id, $columns = array())
 * @method static Model first($columns = array())
 * @method static Model firstOrCreate($attributes, $values = array())
 * @method static Model getModel()
 * @method static Model newModelInstance()
 * @method static Model updateOrCreate($attributes, $values = array())
 * @method static Model|Builder  forceCreate($attributes)
 * @method static Model|Builder create($attributes = array())
 * @method static Model|Collection|static[]|static|null find($id, $columns = array())
 */
abstract class Model extends BaseModel
{
    protected const DAYS_TO_KEEP_SOFT_DELETES = 180;

    /** @var bool */
    protected static $unguardValidation = false;

    /**
     * The attributes that represent enums.
     *
     * @var array
     */
    protected $enums = [];

    /**
     * The key to be used for the view error bag.
     *
     * @var string
     */
    protected $errorBag = 'default';

    /** {@inheritdoc} */
    protected static function boot(): void
    {
        parent::boot();

        static::saving(function ($model) {
            return $model->validate();
        });

        static::deleting(function (self $model): void {
            if (method_exists($model, 'getHasRelationsAttribute')
                && ((property_exists($model, 'forceDeleting') && !$model->{'forceDeleting'}) ||
                    !property_exists($model, 'forceDeleting'))
                && $model->getAttribute('has_relations')
            ) {
                throw new Exception('Model can\'t be deleted, it has relations.');
            }
        });
    }

    public function validate(): void
    {
        if (static::$unguardValidation) {
            return;
        }

        if (!empty($this->rules())) {
            $instance = $this->getValidatorInstance();
            if (!$instance->passes()) {
                $this->failedValidation($instance);
            }
        }
    }

    protected function getValidatorInstance(): Validator
    {
        $factory = app(ValidationFactory::class);

        if (method_exists($this, 'validator')) {
            return app()->call([$this, 'validator'], compact('factory'));
        }

        return $this->createDefaultValidator($factory);
    }

    protected function createDefaultValidator(ValidationFactory $factory): Validator
    {
        return $factory->make(
            $this->validationData(),
            $this->rules()
        );
    }

    /** Get the validation rules that apply to the Model. */
    public function rules(): array
    {
        return [];
    }

    /** Get data to be validated from the Model. */
    protected function validationData(): array
    {
        return $this->attributes;
    }

    protected function failedValidation(Validator $validator): void
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();

        foreach ($this->enums as $key => $value) {
            $attributes[$key] = optional($this->getAttribute($key))->toArray();
        }

        return $attributes;
    }

    /**
     * Get the array of declared enum properties.
     *
     * @return array The declared enum properties.
     *
     * @codeCoverageIgnore
     */
    public static function enums(): array
    {
        return (new static)->enums;
    }

    /**
     * {@inheritdoc}
     */
    public static function __callStatic($method, $parameters)
    {
        if (Str::startsWith($method, 'random')) {
            return static::random(Str::snake(Str::substr($method, 6)));
        }

        return parent::__callStatic($method, $parameters);
    }

    public static function isHidden(string $property): bool
    {
        return in_array($property, (new static)->getHidden());
    }

    public static function hasColumn(string $column): bool
    {
        return Schema::hasColumn((new static)->getTable(), $column);
    }

    /** {@inheritdoc} */
    public function getAttribute($key)
    {
        if ($key === null) {
            return null;
        }

        if (array_key_exists($key, $this->enums)) {
            $attributeValue = $this->getAttributeFromArray($key);

            return $attributeValue !== null ?
                forward_static_call([$this->enums[$key], 'fromValue'], $attributeValue) :
                null;
        }

        return parent::getAttribute($key);
    }

    /** {@inheritdoc} */
    public function setAttribute($key, $value)
    {
        if ($key === null) {
            return $this;
        }

        if (array_key_exists($key, $this->enums)) {
            return $this->setEnumAttribute($key, $value);
        }

        return parent::setAttribute($key, $value);
    }

    /**
     * @param mixed $value
     *
     * @return Model|BaseModel
     *
     * @throws ValidationException
     */
    protected function setEnumAttribute(string $key, $value)
    {
        if ($value !== null && !$value instanceof $this->enums[$key]) {
            if (is_array($value) && array_key_exists('value', $value)) {
                $value = ctype_digit($value['value']) ? (int) $value['value'] : $value['value'];
            }

            try {
                $value = forward_static_call([$this->enums[$key], 'fromValue'], $value);
            } catch (InvalidArgumentException $exception) {
                throw ValidationException::withMessages([
                    $key => [
                        trans(
                            'validation.enum',
                            ['attribute' => $key, 'other' => class_basename($this->enums[$key])]
                        ),
                    ],
                ]);
            }
        }

        $this->attributes[$key] = $value ? $value->getValue() : null;

        return $this;
    }

    /**
     * @param string|bool $value Value for the field.
     */
    protected function setBoolAttribute(string $key, $value): void
    {
        $this->attributes[$key] = $value === true || $value === 'true';
    }

    /**
     * @return bool|string
     */
    public static function getRepositoryClassName()
    {
        $className = class_basename(static::class);
        $class = $className . 'RepositoryInterface';
        $repository = 'App\\Repositories\\' . $className . '\\' . $class;

        if (!interface_exists($repository)) {
            return false;
        }

        return $repository;
    }

    public static function getRepository()
    {
        $repository = static::getRepositoryClassName();

        if (!$repository) {
            return null;
        }

        return app($repository);
    }

    /** Get the primary keys for the model, can be handy if model used a key pair.*/
    public function getKeys() :array
    {
        return [$this->getKeyName()];
    }

    /**
     * Create or update a record matching the attributes, and fill it with values.
     * Force mass assignment.
     */
    public static function forceUpdateOrCreate(array $attributes, array $values = []): self
    {
        $model = new static();

        return $model::unguarded(function () use ($attributes, $values, $model) {
            return $model->updateOrCreate($attributes, $values);
        });
    }

    public static function unguardValidation(): void
    {
        static::$unguardValidation = true;
    }

    public static function guardValidation(): void
    {
        static::$unguardValidation = false;
    }

    public function resolveRouteBinding($value, $field = null): ?BaseModel
    {
        return $this->where($this->getQualifiedRouteKeyName(), $value)->first();
    }

    protected function getQualifiedRouteKeyName(): string
    {
        return $this->qualifyColumn($this->getRouteKeyName());
    }
}
